#MyRetail

To run locally the following are required

* Java 8
* Docker Compose

##Setup:
* In the project directory run
```
docker-compose up -d
```

* This will start cassandra in a docker container.  Give it a second to fully start up

* If this is your first time running the cassandra container, create the keyspace by running
```
scripts/createKeyspace.sh
```

* Run the project with
```
./gradlew run
```

##Usage:
By default the application will use http://redsky.target.com/ to retrieve prodct names.  If the product id doesn't exist there
GET requests will fail with a 404.

The database initially contains no prices.  If no price exists, GETs will fail with a 404.


To add a price you can use something like:

```
curl -X PUT -H "Content-Type: application/json" localhost:8200/product/51937837 -d '{"id":"51937837","name":"","current_price":{"price":22.30,"currency_code":"USD"}}'
```

Assuming this is successful and the id exists in redsky.target.com, you should be able to perform a GET

```curl localhost:8200/product/51937837 ```


##Testing:
Tests require a running cassandra instance which can be be done using the docker setup above.
Some tests require a mock of the product detail service (redsky).  This is set to run on port 9876 by default.

* Ensure docker is running, following the setup above
* run
```
./gradlew check
```


