package com.myretail.product

import com.fasterxml.jackson.annotation.JsonProperty
import com.myretail.Model
import com.myretail.pricing.Price
class Product implements Model {
	String id
	String name

	@JsonProperty("current_price")
	Price currentPrice
}
