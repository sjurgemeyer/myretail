package com.myretail.product

import com.myretail.error.MyRetailException
import com.myretail.error.ValidationException
import com.myretail.validation.Validator

import groovy.transform.CompileDynamic
import groovy.util.logging.Slf4j

import ratpack.groovy.handling.GroovyChainAction
import ratpack.handling.Context
import ratpack.http.Status
import ratpack.jackson.Jackson

import javax.inject.Inject
@Slf4j
@CompileDynamic
class ProductEndpoint extends GroovyChainAction {
	private final ProductService productService

	@Inject
	ProductEndpoint(ProductService productService) {
		this.productService = productService
	}
	@Override
	void execute() throws Exception {

		prefix(':productId') {
			path {
				String productId = allPathTokens.get("productId")
				try {
					Validator.validateProductId(productId)
					byMethod {
						get {
							render productService.getProduct(productId).onError { Exception e -> handleError(context, e) }
						}

						put {
							parse(Jackson.fromJson(Product))
							.flatMap { Product product ->
								Validator.validateProduct(product)
								product.id = productId //TODO, should probably validate that there aren't mismatched IDs
								productService.saveProduct(product)
							}
							.onError { Exception e -> handleError(context, e) }
							.then {
								response.status(Status.of(204))
								response.send()
							}
						}
					}
				} catch (ValidationException e) {
					handleError(context, e)
				}
			}
		}
	}

	private void handleError(Context context, Exception e) {
		if (e instanceof MyRetailException) {
			context.render e
		} else {
			log.error("Unexpected Error in ProductEndpoint", e)
			context.render new MyRetailException("Unexpected Exception occurred")
		}
	}
}
