package com.myretail.product

import com.myretail.detail.DetailClientService
import com.myretail.detail.ProductDetail
import com.myretail.pricing.Price
import com.myretail.pricing.PricingDao

import ratpack.exec.Promise
import ratpack.exec.util.ParallelBatch
import ratpack.service.Service

import javax.inject.Inject
class ProductService implements Service {

	private final PricingDao pricingDao
	private final DetailClientService detailClientService

	@Inject
	ProductService(
		PricingDao pricingDao,
		DetailClientService detailClientService
	) {
		this.pricingDao = pricingDao
		this.detailClientService = detailClientService
	}

	Promise<Product> getProduct(String id) {

		Product product = new Product(id: id)
		List<Promise> promises = [
			pricingDao.getPricing(id),
			detailClientService.getProductDetail(id)
		]

		ParallelBatch.of(promises)
		.yield()
		.map { List results ->
			Price price = (Price) results[0]
			product.currentPrice = price

			ProductDetail detail = (ProductDetail) results[1]
			product.name = detail.name

			return product
		}
	}

	Promise<Void> saveProduct(Product product) {
		//This could also update the name in the other service
		pricingDao.setPricing(
			product.id,
			product.currentPrice
		)
	}
}
