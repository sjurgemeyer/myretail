package com.myretail

import com.fasterxml.jackson.databind.ObjectMapper

import ratpack.handling.Context
import ratpack.render.RendererSupport
class ModelRenderer extends RendererSupport<Model> {

	ObjectMapper objectMapper = new ObjectMapper()

	@Override
	void render(Context ctx, Model model) throws Exception {
		ctx.render objectMapper.writeValueAsString(model)
	}
}
