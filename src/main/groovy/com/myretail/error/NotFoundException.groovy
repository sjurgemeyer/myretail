package com.myretail.error
class NotFoundException extends MyRetailException {

	NotFoundException(String message) {
		super(message)
	}

	NotFoundException(Exception e) {
		super(e)
	}

}
