package com.myretail.error

import ratpack.handling.Context
import ratpack.http.Status
import ratpack.render.RendererSupport
class MyRetailExceptionRenderer extends RendererSupport<MyRetailException> {

	@Override
	void render(Context ctx, MyRetailException exception) throws Exception {
		switch (exception.class) {
			case NotFoundException:
				ctx.render(new ErrorResponse(exception.message, Status.of(404)))
				break
			case ValidationException:
				ctx.render(new ErrorResponse(exception.message, Status.of(422)))
				break
			default:
				ctx.render(new ErrorResponse(exception.message, Status.of(500)))
		}
	}
}
