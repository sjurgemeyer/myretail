package com.myretail.error
class MyRetailException extends Exception {

	MyRetailException(String message) {
		super(message)
	}

	MyRetailException(Exception e) {
		super(e)
	}

	MyRetailException(String message, Exception e) {
		super(message, e)
	}

}
