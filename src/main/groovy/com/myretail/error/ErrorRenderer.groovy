package com.myretail.error

import groovy.json.JsonOutput

import ratpack.handling.Context
import ratpack.render.RendererSupport

class ErrorRenderer extends RendererSupport<ErrorResponse> {

	@Override
	void render(Context ctx, ErrorResponse response) throws Exception {
		ctx.response.status(response.status)
		ctx.response.send(JsonOutput.toJson(message:response.error))
	}
}
