package com.myretail.error
class ValidationException extends MyRetailException {
	ValidationException(String message) {
		super(message)
	}
}
