package com.myretail.error

import ratpack.http.Status
class ErrorResponse {
	String error
	Status status

	ErrorResponse(String error, Status status = Status.of(500)) {
		this.error = error
		this.status = status
	}
}
