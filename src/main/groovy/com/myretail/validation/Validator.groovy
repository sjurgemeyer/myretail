package com.myretail.validation

import com.myretail.error.ValidationException
import com.myretail.pricing.Price
import com.myretail.product.Product
class Validator {

	static void validateProductId(String productId) {
		//Not sure if this is actually the definition of a product id, but good enough for some basic validation
		if (!(productId ==~ /\d{8}/)) {
			throw new ValidationException("Invalid productId $productId")
		}
	}

	static void validateProduct(Product product) {
		validateProductId(product.id)
		validatePrice(product.currentPrice)
	}

	static void validatePrice(Price price) {
		if (!price) {
			throw new ValidationException("Price is required")
		}
		if (!price.currencyCode || price.currencyCode.size() != 3) {
			throw new ValidationException("Price currencyCode must be 3 characters")
		}
		if (!price.price || price.price <= 0) {
			throw new ValidationException("Price must be present and greater than 0")
		}
	}
}
