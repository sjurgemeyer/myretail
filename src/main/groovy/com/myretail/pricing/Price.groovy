package com.myretail.pricing

import com.fasterxml.jackson.annotation.JsonProperty
class Price {
	BigDecimal price

	@JsonProperty("currency_code")
	String currencyCode
}
