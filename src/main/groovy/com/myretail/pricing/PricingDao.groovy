package com.myretail.pricing

import com.datastax.driver.core.BoundStatement
import com.datastax.driver.core.ConsistencyLevel
import com.datastax.driver.core.PreparedStatement
import com.datastax.driver.core.ResultSet
import com.datastax.driver.core.Row
import com.myretail.PricingCassandraConfig
import com.myretail.error.NotFoundException

import groovy.util.logging.Slf4j

import ratpack.exec.Promise

import smartthings.ratpack.cassandra.CassandraService

import javax.inject.Inject

@Slf4j
class PricingDao {
	private static final String FIND_BY_PRODUCT_ID = "SELECT * FROM product_price where product_id = ?"
	private static final String SAVE_PRICE = "INSERT INTO product_price (product_id, price, currency_code) values (?, ?, ?)"

	CassandraService cassandraService
	ConsistencyLevel consistencyLevel = ConsistencyLevel.LOCAL_QUORUM

	@Inject
	PricingDao(CassandraService cassandraService, PricingCassandraConfig config) {
		this.cassandraService = cassandraService
		if (config.consistencyLevel) {
			this.consistencyLevel = ConsistencyLevel.valueOf(config.consistencyLevel)
		}
	}

	Promise<Price> getPricing(String productId) {

		PreparedStatement preparedStatement = cassandraService.session.prepare(FIND_BY_PRODUCT_ID)
		BoundStatement boundStatement = preparedStatement.bind(productId)
		boundStatement.setConsistencyLevel(consistencyLevel)

		cassandraService.execute(boundStatement)
		.map { ResultSet resultSet ->
			if (resultSet.isExhausted()) {
				log.warn("No product pricing was found for $productId")
				throw new NotFoundException("No product pricing was found")
			}

			log.trace("Successfully retrieved pricing for $productId")
			Row row = resultSet.one()
			new Price(
				price: row.getDecimal('price'),
				currencyCode: row.getString('currency_code')
			)
		}
	}

	Promise<Void> setPricing(String productId, Price price) {

		PreparedStatement preparedStatement = cassandraService.session.prepare(SAVE_PRICE)
		BoundStatement boundStatement = preparedStatement.bind(
			productId,
			price.price,
			price.currencyCode,
		)
		boundStatement.setConsistencyLevel(consistencyLevel)

		cassandraService.execute(boundStatement)
		.map { ResultSet resultSet ->
			log.trace("Successfully updated pricing for $productId")
		}
	}
}
