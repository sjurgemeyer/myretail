package com.myretail.detail

import com.myretail.DetailServiceConfig
import com.myretail.error.MyRetailException
import com.myretail.error.NotFoundException

import groovy.util.logging.Slf4j

import io.netty.handler.codec.http.HttpHeaderNames

import ratpack.exec.Promise
import ratpack.func.Action
import ratpack.http.client.HttpClient
import ratpack.http.client.ReceivedResponse
import ratpack.http.client.RequestSpec

import java.time.Duration

import javax.inject.Inject
@Slf4j
class DetailClientService {
	private final HttpClient httpClient
	private final String detailServiceUrl
	private final Duration timeout

	@Inject
	DetailClientService(HttpClient httpClient,
						DetailServiceConfig config
	) {
		this.httpClient = httpClient
		this.detailServiceUrl = config.url
		this.timeout = Duration.ofSeconds(config.timeout ?: 10)
	}

	Promise<ProductDetail> getProductDetail(String id) {
		URI requestUri = URI.create( "http://$detailServiceUrl/$id")

		httpClient.get(requestUri, { RequestSpec request ->
			request.headers.set(HttpHeaderNames.ACCEPT, '*/*')
			request.connectTimeout(timeout)
		} as Action)
		.map({ ReceivedResponse response ->
			switch (response.statusCode) {
				case 404:
					throw new NotFoundException("Product Detail not found")
				case { ((int)it) >= 300 } :
					throw new MyRetailException ("Could not retrieve product detail")
				default:
					return DetailParser.parse(response.body.text)
			}
		})
	}
}
