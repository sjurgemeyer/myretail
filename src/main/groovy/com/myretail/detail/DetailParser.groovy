package com.myretail.detail

import com.myretail.error.MyRetailException

import groovy.json.JsonSlurper
import groovy.transform.CompileDynamic
import groovy.util.logging.Slf4j
@CompileDynamic
@Slf4j
class DetailParser {
	static ProductDetail parse(String body) {
		def parsedDetail
		try {
			 parsedDetail = new JsonSlurper().parseText(body)
		} catch (Exception e) {
			log.error("Error parsing product detail", e)
			throw new MyRetailException("Invalid product detail data found", e)
		}

		String name = parsedDetail?.product?.item?.product_description?.title
		//We'll say that not having a valid name is bad data
		if (!name) {
			throw new MyRetailException("Invalid product detail data found")
		}

		return new ProductDetail(
			name: name
		)
	}
}
