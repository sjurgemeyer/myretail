import com.google.common.io.Resources
import com.myretail.DetailServiceConfig
import com.myretail.ModelRenderer
import com.myretail.PricingCassandraConfig
import com.myretail.error.ErrorRenderer
import com.myretail.error.MyRetailExceptionRenderer
import com.myretail.product.ProductEndpoint

import ratpack.config.ConfigData

import smartthings.ratpack.cassandra.CassandraMigrationModule
import smartthings.ratpack.cassandra.CassandraModule

import static ratpack.groovy.Groovy.ratpack

ratpack {
	URL resource = Resources.getResource("myRetailConfig.yml"); //TODO allow system override
	ConfigData configData = ConfigData.builder().yaml(resource).build()
	serverConfig {
		port configData.get('/ratpack/port', Integer)
	}
	bindings {

		bindInstance(ConfigData, configData)
		bindInstance(PricingCassandraConfig, configData.get('/pricing', PricingCassandraConfig))
		bindInstance(DetailServiceConfig, configData.get('/detail', DetailServiceConfig))

		moduleConfig(CassandraModule, configData.get('/cassandra', CassandraModule.Config))
		moduleConfig(CassandraMigrationModule, configData.get('/cassandra', CassandraModule.Config))

		bind(ProductEndpoint)
		bind(ErrorRenderer)
		bind(MyRetailExceptionRenderer)
		bind(ModelRenderer)
	}
	handlers {
		prefix('product') {
			all chain(registry.get(ProductEndpoint))
		}
	}
}
