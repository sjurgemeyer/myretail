package com.myretail.error

import ratpack.handling.Context

import spock.lang.Specification
import spock.lang.Unroll

class MyRetailExceptionRendererSpec extends Specification {

	@Unroll
	def 'should create create error response'() {
		setup:
		MyRetailExceptionRenderer renderer = new MyRetailExceptionRenderer()
		Context context = Mock()

		when:
		renderer.render(context, exception)

		then:
		1 * context.render({ ErrorResponse error ->
			assert error.status.code == statusCode
			assert error.error == exception.message
			true
		})

		0 * _

		where:
		exception                          | statusCode
		new NotFoundException("not found") | 404
		new ValidationException("bad")     | 422
		new MyRetailException("something") | 500
	}
}
