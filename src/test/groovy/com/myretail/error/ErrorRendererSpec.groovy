package com.myretail.error

import ratpack.handling.Context
import ratpack.http.Response
import ratpack.http.Status

import spock.lang.Specification
import spock.lang.Unroll

class ErrorRendererSpec extends Specification {

	@Unroll
	def 'should create standarad error response'() {
		setup:
		Status status = Status.of(statusCode)
		ErrorRenderer renderer = new ErrorRenderer()
		Context context = Mock()
		Response response = Mock()
		ErrorResponse errorResponse = new ErrorResponse(
			message,
			status
		)

		when:
		renderer.render(context, errorResponse)

		then:
		_ * context.getResponse() >> response
		1 * response.status(status)
		1 * response.send(expectedBody)
		0 * _

		where:
		statusCode | message              | expectedBody
		500        | "something happened" | '{"message":"something happened"}'
		404        | "not found"          | '{"message":"not found"}'
	}
}
