package com.myretail.product

import com.myretail.detail.DetailClientService
import com.myretail.detail.ProductDetail
import com.myretail.pricing.Price
import com.myretail.pricing.PricingDao

import ratpack.exec.ExecResult
import ratpack.exec.Promise
import ratpack.test.exec.ExecHarness

import spock.lang.Specification

class ProductServiceSpec extends Specification {

	ExecHarness harness = ExecHarness.harness()

	ProductService productService
	PricingDao pricingDao = Mock()
	DetailClientService detailClientService = Mock()
	void setup() {
		productService = new ProductService(
			pricingDao,
			detailClientService
		)

		0 * _
	}

	def 'should get product information'() {
		setup:
		String productId = '11111111'
		Price expectedPrice = new Price(
			price: 20.99,
			currencyCode: 'USD'
		)
		ProductDetail expectedProductDetail = new ProductDetail(
			name: 'myproduct'
		)

		when:
		ExecResult result = harness.yield {
			productService.getProduct(productId)
		}

		then:
		!result.isError()

		Product product = result.value
		product.currentPrice == expectedPrice
		product.name == expectedProductDetail.name
		product.id == productId

		1 * pricingDao.getPricing(productId) >> Promise.value(expectedPrice)
		1 * detailClientService.getProductDetail(productId) >> Promise.value(expectedProductDetail)
	}

	def 'should save product'() {
		setup:
		Product product = new Product(
			id: '22222222',
			currentPrice: new Price(
				price: 100.00,
				currencyCode: 'USD'
			)
		)
		when:
		ExecResult result = harness.yield {
			productService.saveProduct(product)
		}

		then:
		!result.isError()
		1 * pricingDao.setPricing(product.id, product.currentPrice) >> Promise.value(null)
	}

}
