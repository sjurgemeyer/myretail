package com.myretail.product

import com.fasterxml.jackson.databind.ObjectMapper
import com.myretail.pricing.Price
import com.myretail.test.MyRetailApplicationUnderTest

import groovy.json.JsonSlurper

import ratpack.groovy.test.embed.GroovyEmbeddedApp
import ratpack.http.client.RequestSpec
import ratpack.server.ServerConfig
import ratpack.test.ApplicationUnderTest
import ratpack.test.embed.EmbeddedApp
import ratpack.test.http.TestHttpClient

import spock.lang.Specification
import spock.lang.Unroll
class ProductEndpointSpec extends Specification {

	ObjectMapper objectMapper = new ObjectMapper()
	EmbeddedApp productDetailMock
	int productDetailPort = 9876

	ApplicationUnderTest applicationUnderTest = new MyRetailApplicationUnderTest()

	@Delegate
	TestHttpClient testClient = applicationUnderTest.httpClient

	def cleanup() {
		productDetailMock?.close()
	}

	def 'invalid product id returns error'() {
		given:

		when:
		get('/product/abcd')

		then:
		response.statusCode == 422
		new JsonSlurper().parseText(response.body.text).message == 'Invalid productId abcd'

	}

	@Unroll
    def "Invalid product expects message #expectedMessage"() {
		given:
		Product product = new Product(
			id: productId,
			name: name,
			currentPrice: new Price(price: price, currencyCode: currencyCode))

		requestSpec { RequestSpec requestSpec ->
			requestSpec.body.type('application/json')
			requestSpec.body.bytes(objectMapper.writeValueAsString(product).bytes)
		}

		when:
		put("/product/${urlId}")

		then:
		response.statusCode == 422
		new JsonSlurper().parseText(response.body.text).message == expectedMessage

		where:
		urlId      | productId  | price | currencyCode | expectedMessage
		'1111111' | '11111111' | 100   | 'USD'         | 'Invalid productId 1111111'
		'11111111' | '1111111'  | 100   | 'USD'        | 'Invalid productId 1111111'
		'11111111' | null       | 100   | 'USD'        | 'Invalid productId null'
		'11111111' | '11111111' | null  | 'USD'        | 'Price must be present and greater than 0'
		'11111111' | '11111111' | -100  | 'USD'        | 'Price must be present and greater than 0'
		'11111111' | '11111111' | 100   | 'US'         | 'Price currencyCode must be 3 characters'
		'11111111' | '11111111' | 100   | null         | 'Price currencyCode must be 3 characters'


	}

	def 'put and get product works'() {
		given:
		mockProductDetailService('my product')
		Product product = new Product(
			id: '11111111',
			name: 'my product',
			currentPrice: new Price(price: 100.22, currencyCode: 'USD'))

		requestSpec { RequestSpec requestSpec ->
			requestSpec.body.type('application/json')
			requestSpec.body.bytes(objectMapper.writeValueAsString(product).bytes)
		}

		when:
		put("/product/${product.id}")

		then:
		response.statusCode == 204

		when:
		get("/product/${product.id}")

		then:
		response.statusCode == 200
		Product result = objectMapper.readValue(response.body.text, Product)
		result.id == product.id
		result.name == product.name
		result.currentPrice.price == product.currentPrice.price
		result.currentPrice.currencyCode == product.currentPrice.currencyCode

		when: 'Updating price'
		product.currentPrice.price = 200.01
		put("/product/${product.id}")

		then:
		response.statusCode == 204

		when:
		get("/product/${product.id}")

		then: 'price update is reflected on read'
		response.statusCode == 200
		Product newResult = objectMapper.readValue(response.body.text, Product)
		newResult.currentPrice.price == 200.01

	}

	@Unroll
	def 'product detail error code #detailStatus results in status #returnedStatus'() {

		given:
		mockProductDetailServiceError(detailStatus)
		Product product = new Product(
			id: '11111111',
			name: 'my product',
			currentPrice: new Price(price: 100.22, currencyCode: 'USD'))

		requestSpec { RequestSpec requestSpec ->
			requestSpec.body.type('application/json')
			requestSpec.body.bytes(objectMapper.writeValueAsString(product).bytes)
		}

		when:
		put("/product/${product.id}")

		then:
		response.statusCode == 204

		when:
		get("/product/${product.id}")

		then:
		response.statusCode == returnedStatus

		where:
		detailStatus | returnedStatus
		404          | 404
		403          | 500
		500          | 500

	}

	private void mockProductDetailService(String name) {
		productDetailMock = GroovyEmbeddedApp.fromServer(
			ServerConfig.builder()
					.address(InetAddress.getLoopbackAddress())
					.port(productDetailPort)
					.build(),
			{
				handlers {
					all {
						response.status(200)
						render """{"product":{"item":{"product_description":{"title":"$name"}}}}"""
					}
				}
			}
		)

		//Calling getAddress starts the embedded app
		productDetailMock.address
	}

	private void mockProductDetailServiceError(int statusCode) {
		productDetailMock = GroovyEmbeddedApp.fromServer(
			ServerConfig.builder()
					.address(InetAddress.getLoopbackAddress())
					.port(productDetailPort)
					.build(),
			{
				handlers {
					all {
						response.status(statusCode)
						response.send()
					}
				}
			}
		)

		//Calling getAddress starts the embedded app
		productDetailMock.address
	}

}
