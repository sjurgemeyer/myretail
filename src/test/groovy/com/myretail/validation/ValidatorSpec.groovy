package com.myretail.validation

import com.myretail.error.ValidationException
import com.myretail.pricing.Price
import com.myretail.product.Product

import spock.lang.Specification
import spock.lang.Unroll

class ValidatorSpec extends Specification {

	@Unroll
	def '#productId validation should pass'() {
		when:
		Validator.validateProductId(productId)

		then:
		notThrown Exception

		where:
		productId << [
			'12312312',
			'00000000'
		]
}

	@Unroll
	def '#productId validation should not pass'() {

		when:
		Validator.validateProductId(productId)

		then:
		thrown ValidationException

		where:
		productId << [
			'A0000000',
			'A-00000000',
			'000000000',
			'0000000',
			'',
			null
		]
	}

	@Unroll
	def 'validation should pass'() {

		setup:
		Product product = new Product(
			id: id,
			currentPrice: new Price(
				price: price,
				currencyCode: currencyCode
			)
		)

		when:
		Validator.validateProduct(product)

		then:
		notThrown Exception

		where:
		id         | price   | currencyCode
		'12312312' | 0.11    | 'USD'
		'00000000' | 2000000 | 'EUR'

	}

	@Unroll
	def 'validation should not pass with #scenario'() {

		setup:
		Product product = new Product(
			id: id,
			currentPrice: new Price(
				price: price,
				currencyCode: currencyCode
			)
		)

		when:
		Validator.validateProduct(product)

		then:
		thrown ValidationException

		where:
		id         | price    | currencyCode | scenario
		'12312312' | 0.11     | null         | 'missing currencyCode'
		'00000000' | null     | 'EUR'        | 'missing price'
		null       | 0.22     | 'EUR'        | 'missing productId'
		'1231231'  | 0.11     | 'USD'        | 'bad productId'
		'00000000' | -2000000 | 'EUR'        | 'bad price'
		'00000000' | 2000000  | 'Dollars'    | 'bad currencyCode'

	}
}
