package com.myretail.pricing

import com.datastax.driver.core.BoundStatement
import com.datastax.driver.core.ConsistencyLevel
import com.datastax.driver.core.PreparedStatement
import com.datastax.driver.core.ResultSet
import com.datastax.driver.core.Row
import com.datastax.driver.core.Session
import com.myretail.PricingCassandraConfig
import com.myretail.error.NotFoundException

import ratpack.exec.ExecResult
import ratpack.exec.Promise
import ratpack.test.exec.ExecHarness

import smartthings.ratpack.cassandra.CassandraService

import spock.lang.Specification

class PricingDaoSpec extends Specification {

	ExecHarness harness = ExecHarness.harness()

	CassandraService cassandraService = Mock()
	Session session = Mock()
	PreparedStatement preparedStatement = Mock()
	BoundStatement boundStatement = Mock()
	ResultSet resultSet = Mock()
	Row row = Mock()
	PricingCassandraConfig config = Mock()
	PricingDao pricingDao

	void setup() {
		pricingDao = new PricingDao(
			cassandraService,
			config
		)

		0 * _
	}

	def 'should get pricing from cassandra'() {
		setup:
		String productId = '12312312'
		BigDecimal expectedPrice = 100.01
		String expectedCurrencyCode = 'USD'

		when:
		ExecResult result = harness.yield {
			pricingDao.getPricing(productId)
		}

		then:
		!result.isError()

		Price price = result.value
		price.price == expectedPrice
		price.currencyCode == expectedCurrencyCode

		1 * cassandraService.getSession() >> session
		1 * session.prepare(_) >> preparedStatement
		1 * preparedStatement.bind(productId) >> boundStatement
		1 * boundStatement.setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)
		1 * cassandraService.execute(boundStatement) >> Promise.value(resultSet)
		1 * resultSet.isExhausted() >> false
		1 * resultSet.one() >> row
		1 * row.getDecimal('price') >> expectedPrice
		1 * row.getString('currency_code') >> expectedCurrencyCode
	}

	def 'should throw NotFoundException when no results are found'() {
		setup:
		String productId = '12312312'

		when:
		ExecResult result = harness.yield {
			pricingDao.getPricing(productId)
		}

		then:
		result.isError()
		result.throwable instanceof NotFoundException

		1 * cassandraService.getSession() >> session
		1 * session.prepare(_) >> preparedStatement
		1 * preparedStatement.bind(productId) >> boundStatement
		1 * boundStatement.setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)
		1 * cassandraService.execute(boundStatement) >> Promise.value(resultSet)
		1 * resultSet.isExhausted() >> true
	}

	def 'should set pricing in cassandra'() {
		setup:
		String productId = '12312312'
		Price price = new Price(
			price: 100.01,
			currencyCode: "USD"
		)

		when:
		ExecResult result = harness.yield {
			pricingDao.setPricing(productId, price)
		}

		then:
		!result.isError()

		1 * cassandraService.getSession() >> session
		1 * session.prepare(_) >> preparedStatement
		1 * preparedStatement.bind(productId, price.price, price.currencyCode) >> boundStatement
		1 * boundStatement.setConsistencyLevel(ConsistencyLevel.LOCAL_QUORUM)
		1 * cassandraService.execute(boundStatement) >> Promise.value(resultSet)
	}
}
