package com.myretail.detail

import com.myretail.DetailServiceConfig
import com.myretail.error.MyRetailException
import com.myretail.error.NotFoundException

import io.netty.handler.codec.http.HttpHeaderNames

import ratpack.exec.ExecResult
import ratpack.exec.Promise
import ratpack.http.MutableHeaders
import ratpack.http.TypedData
import ratpack.http.client.HttpClient
import ratpack.http.client.ReceivedResponse
import ratpack.http.client.RequestSpec
import ratpack.test.exec.ExecHarness

import spock.lang.Specification
import spock.lang.Unroll

import java.time.Duration

class DetailClientServiceSpec extends Specification {

	DetailClientService detailClientService
	DetailServiceConfig config = new DetailServiceConfig(
		url: "testUrl",
		timeout: 2
	)

	ExecHarness harness = ExecHarness.harness()

	HttpClient client = Mock()
	RequestSpec request = Mock()
	MutableHeaders headers = Mock()
	RequestSpec.Body body = Mock()
	ReceivedResponse response = Mock()
	TypedData responseBody = Mock()


	void setup() {
		detailClientService = new DetailClientService(
			client,
			config
		)
		_ * request.getHeaders() >> headers
		_ * request.getBody() >> body

		_ * response.getBody() >> responseBody

		0 * _
	}

	def 'should retrieve detail'() {
		setup:
		String productId = "11111111"

		when:
		ExecResult result = harness.yield {
			detailClientService.getProductDetail(productId)
		}

		then:
		!result.isError()
		ProductDetail detail = result.value
		detail.name == "The Rolling Stones: Some Girls - Live in Texas '78"

		1 * client.get(new URI('http://testUrl/11111111'), _) >> { url, closure ->
			closure.execute(request)
			return Promise.value(response)
		}
		1 * headers.set(HttpHeaderNames.ACCEPT, '*/*')
		1 * request.connectTimeout(Duration.ofSeconds(2))

		_ * response.getStatusCode() >> 200
		//Response retrieved from http://redsky.target.com/v1/pdp/tcin/13860419
		1 * responseBody.getText() >> '''
			{"product":{"rating_and_review_reviews":{"hasErrors":false,"offset":0,"totalResults":1,"locale":"en_US","limit":10,"duration":2,"result":[{"TagDimensions":{},"TagDimensionsOrder":[],"AdditionalFieldsOrder":[],"IsRatingsOnly":false,"UserNickname":"Jason","Photos":[],"ContextDataValues":{},"Videos":[],"ContextDataValuesOrder":[],"LastModificationTime":"2014-06-04T17:00:08.000+00:00","TotalFeedbackCount":0,"TotalPositiveFeedbackCount":0,"BadgesOrder":[],"UserLocation":"Portland ,OR","Badges":{},"AuthorId":"103239143","IsFeatured":false,"SecondaryRatingsOrder":["Entertaining","Features","Value"],"IsSyndicated":false,"ProductRecommendationIds":[],"Title":"the stones rule","ProductId":"13860419","AdditionalFields":{},"TotalNegativeFeedbackCount":0,"SubmissionTime":"2011-11-23T12:42:04.000+00:00","Rating":5,"ContentLocale":"en_US","RatingRange":5,"TotalCommentCount":0,"ReviewText":"in this concert the stones cant be outdone they are the best at what they do and prove it,high energy classic mick jagger and keith richards style show you will be entertained","ModerationStatus":"APPROVED","ClientResponses":[],"Id":"15187838","CommentIds":[],"SecondaryRatings":{"Value":{"Value":5,"Label":"value","Id":"Value","ValueRange":5,"DisplayType":"NORMAL"},"Entertaining":{"Value":5,"Label":"entertaining","Id":"Entertaining","ValueRange":5,"DisplayType":"NORMAL"},"Features":{"Value":5,"Label":"features","Id":"Features","ValueRange":5,"DisplayType":"NORMAL"}},"LastModeratedTime":"2014-06-04T17:00:08.000+00:00"}]},"deep_red_labels":{"total_count":2,"labels":[{"count":0,"id":"gqwm8i","name":"TAC","type":"relationship type","priority":0},{"count":0,"id":"twbl94","name":"Movies","type":"merchandise type","priority":0}]},"promotion":{"subscriptionShippingMessage":"free shipping","callout":{"text":"","type":{"appliedTo":"","description":""}},"promotionList":[]},"question_answer_statistics":{"questionCount":0,"answerCount":0},"bulk_ship":{"tcins":[{"tcin":"13860419","handlingItemCharge":{"itemBulky":false}}]},"rating_and_review_statistics":{"hasErrors":false,"result":{"13860419":{"coreStats":{"AverageOverallRating":5.0,"SecondaryRatingsAveragesOrder":[],"OverallRatingRange":5,"SecondaryRatingsAverages":{},"RatingDistribution":[{"Count":0,"RatingValue":1},{"Count":0,"RatingValue":2},{"Count":0,"RatingValue":3},{"Count":0,"RatingValue":4},{"Count":1,"RatingValue":5}],"TotalReviewCount":1,"RatingsOnlyReviewCount":0,"RatingReviewTotal":1},"secondaryRatingFields":[],"expertReviewContentMetadata":[]}}},"available_to_promise_network":{"product_id":"13860419","id_type":"TCIN","available_to_promise_quantity":18.0,"street_date":"2011-11-21T06:00:00.000Z","availability":"AVAILABLE","online_available_to_promise_quantity":18.0,"stores_available_to_promise_quantity":0.0,"availability_status":"IN_STOCK","multichannel_options":[]},"price":{"partNumber":"13860419","channelAvailability":"2","listPrice":{"minPrice":0.0,"maxPrice":0.0,"price":9.99,"formattedPrice":"$9.99","priceType":"List","null":false},"offerPrice":{"minPrice":0.0,"maxPrice":0.0,"price":8.69,"formattedPrice":"$8.69","priceType":"Reg","startDate":1433142000000,"endDate":253402236000000,"saveDollar":"1.30","savePercent":"13","eyebrow":"","null":false},"ppu":"","mapPriceFlag":"N"},"item":{"tcin":"13860419","bundle_components":{"is_assortment":false,"is_kit_master":false},"dpci":"246-07-4199","upc":"801213039494","product_description":{"title":"The Rolling Stones: Some Girls - Live in Texas '78","bullet_description":["<B>Movie Genre:</B> Music","<B>Software Format:</B> DVD","<B>Movie Studio:</B> Eagle Rock"],"general_description":"The Rolling Stones: Some Girls - Live in"},"parent_items":"44365008","buy_url":"http://www.target.com/p/the-rolling-stones-some-girls-live-in-texas-78/-/A-13860419","enrichment":{"images":[{"base_url":"http://target.scene7.com/is/image/Target/","primary":"13860419"}],"sales_classification_nodes":[{"node_id":"55uhr","wcs_id":"471004"}]},"return_method":"This item can be returned to any Target store or Target.com.","handling":{"import_designation_description":"Made in the USA or Imported"},"recall_compliance":{"is_product_recalled":false},"tax_category":{"tax_code_id":99999,"tax_code":"99999"},"display_option":{"is_size_chart":false,"is_warranty":false},"fulfillment":{"is_po_box_prohibited":false},"package_dimensions":{"weight":"0.2","weight_unit_of_measure":"POUND","width":"5.39","depth":"7.51","height":"0.54","dimension_unit_of_measure":"INCH"},"environmental_segmentation":{"is_lead_disclosure":false},"manufacturer":{},"product_classification":{"product_type":"542","product_type_name":"ELECTRONICS","item_type_name":"Movies","item_type":{"category_type":"Item Type: MMBV","type":300752,"name":"Movies"}},"product_brand":{"brand":"Target.com Use Only"},"item_state":"READY_FOR_LAUNCH","specifications":[],"attributes":{"gift_wrapable":"N","has_prop65":"N","is_hazmat":"N","max_order_qty":10,"street_date":"2011-11-21","media_format":"DVD","merch_class":"TCOM MOVIES","merch_subclass":7,"return_method":"This item can be returned to any Target store or Target.com."},"country_of_origin":"US","relationship_type_code":"Title Authority Child","subscription_eligible":false,"estore_item_status_code":"A","return_policies":{}}}}
		'''
	}

	@Unroll
	def 'should throw Exception of type #exceptionType when recieving status code #statusCode'() {
		setup:
		String productId = "11111111"

		when:
		ExecResult result = harness.yield {
			detailClientService.getProductDetail(productId)
		}

		then:
		result.isError()
		result.throwable.class == exceptionType

		1 * client.get(new URI('http://testUrl/11111111'), _) >> { url, closure ->
			closure.execute(request)
			return Promise.value(response)
		}
		1 * headers.set(HttpHeaderNames.ACCEPT, '*/*')
		1 * request.connectTimeout(Duration.ofSeconds(2))

		_ * response.getStatusCode() >> statusCode

		where:
		exceptionType     | statusCode
		NotFoundException | 404
		MyRetailException | 403
		MyRetailException | 500
	}
}
